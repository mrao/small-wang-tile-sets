    #include <sys/types.h>
    #include <sys/stat.h>
    #include <fcntl.h>
    #include <unistd.h>
    #include <string.h>
    #include <stdlib.h>
    #include <assert.h>
    #include <stdio.h>  
  struct edges{
    unsigned char from[N];
    unsigned char to[N];
  };

  struct edges graph;
    struct edges graphs[1000000];
    int nbgraphs = 0;  
  struct edges permuted_graph;


  int a[N];
  int m;
  #define order(i,j)  (a[i] < a[j])
  #define swap(i,j)  { int tmp = a[i]; a[i] = a[j]; a[j] = tmp;}
  #define NOBJ m
  int next_permutation(int p)
  // Find the next permutation whose pth term is different from this one
  // return the first index that was changed
  {      
     int k,l,i,j;
     if (p < 0)
           return -1; 
     if (p >= NOBJ-1)
           {
                  
                  // Find the largest index k such that a[k] < a[k + 1].
                  for (k = NOBJ-2; k >= 0; k--)
                    if (order(k,k+1))
                          break;
                  if (k < 0)
                    // If no such index exists, the permutation is the last permutation.
                    return -1;
                  // Find the largest index l such that a[k] < a[l]. 
                  // Since k + 1 is such an index, l is well defined
                  // and satisfies k < l.
                  for (l = NOBJ-1; !order(k,l); l--);
                  // Swap a[k] with a[l].
                  swap(k,l);
                  // Reverse the sequence from a[k + 1] to a[n].
                  i = k + 1;
                  j = NOBJ - 1;
                  // note: the conditions i >= 0 && j >= 0 are unnecessary
                  // but are here for gcc to be happy
                  while ((i >= 0) && (j >= 0) && (i < j))
                    {
                           swap(i,j);
                           i++;
                           j--;
                    }
                  return k;
           }   
     else
           {
                  // find the smallest element of t[p+1:NOBJ-1] bigger than t[p] if it exists...
                  int i,j,smallest = -1;
                  for (i = p+1; i < NOBJ; i++)
                     if (order(p, i)) 
                        smallest = i;
                  if (smallest == -1)
                    /* not possible, we need to change something up high */
                    return next_permutation(p-1);
                  for (i = p+1; i < NOBJ; i++)
                    if (order(p, i))
                          if (order(i, smallest))                   
                            smallest = i;
                  swap(p,smallest);
                  // sort the remaining...
                  // this is insertion sort
                  for (j = p+1; j < NOBJ; j++)
                    {
                           for (i = j; i > p+1; i--)
                             {
                                if order(i-1,i)
                                          break;
                                    swap(i-1,i);
                             }
                    }
                  return p;
           }       
  }  
  int compar(const void *ptr1v, const void *ptr2v)
  {
     const struct edges *ptr1=(const struct edges *)ptr1v;
     const struct edges *ptr2=(const struct edges *)ptr2v;
    /* decreasing in sources (from), increasing in destination (to) */
     int x = memcmp(ptr2->from, ptr1->from, N);
     if (x == 0)
        return memcmp(ptr1->to, ptr2->to, N);
     else 
       return x;
  }

  int main(){
    int nbgraphs = 0;
    int prev_graphs = 0;
    int part[N+1];
    for (m = 2; m < N-1; m++)
      {
             int j,s,x;
        // H1      
        part[0] = N-m+1; for (j = 1; j < m; j++) part[j] = 1; part[m] = -1;
        for(;;){  
        // H2
          {
	     int arcs = 0;
	     int j,k;
	     for (j = m-1; j >= 0 ; j--)
	     {
	            for (k = 0; k < part[j]; k++)
	                  {
	                     graph.from[arcs+k] = m-1-j;
	                     graph.to[arcs+k] = 0;
	                  }                  
	            arcs+=k;
	     }
	     for (;;)
	     {
	          int j,k;
	  		/* If, for a given vertex, the outgoing vertices
	  		are not in increasing order, we do not treat the graph, as
	  		it could be obtained in another way */
	  		{ int ok = 1;
	  		for (j = 0; j < N-1; j++) 
	  		  if ((graph.from[j] == graph.from[j+1]) && 
	  		      (graph.to[j] > graph.to[j+1])) 
	  			    ok = 0;
	  		if (ok)
	  			 {
				    { 
				      int i;
				      for (i = 0; i < m; i++)
				      {
				        int found = 0;
				            for (j = 0; j < N; j++) if (graph.to[j] == i) found = 1;
				            if (!found) ok = 0;
				      }    
				    }
				    if (ok)
				    {
				     int i,j,k,t;
				     int array[m][m];
				     for (i = 0; i < m; i++) for (j = 0; j < m ; j++) array[i][j] = 0;
				     for (j = 0; j < N; j++){
				       array[graph.from[j]][graph.to[j]]++;
				       }
				      
				     for (j = 0; j < N; j++) /* for each edge (u,v)*/
				     /* Compute all vertices that are accessible from v */
				     {
				       int set[m];
				       for (i = 0; i < m; i++) set[i] = 0;
				       set[graph.to[j]] = 1;
				       for (t = 0; t < m;t++)
				         for (i = 0; i < m; i++)
				           for (k = 0; k < m; k++)
				             if (array[i][k]  && set[i]) set[k] = 1;
				       if (!set[graph.from[j]]) { ok = 0; break;}
				     } 
				    }
				    if (ok)
				    {
				      int i,j;
				      for (i = 0; i < m ; i++) /* for each vertex u */
				      {
				       int u = i, out, v, f = 0;
				       do{
				       /* 
				       compute the outdegree of i, and by the same occasion one of its
				       neighbours
				       */
				       out = 0; 
				       for (j = 0; j < N; j++)
				         if (graph.from[j] == u) {out++; v= graph.to[j];}
				             
				       if (out != 1) {f = 1; break;} /* we found a vertex of outdegree at least 2 */
				       u = v;   
				       } while (u != i);
				       if (!f)  {ok = 0; break;}
				    }}   
				 }
	  		if (ok)
	  			 {
				   int i;
				   int found;
				    
				   for (i = 0; i < m; i++)
				     a[i] = i;
				   found = 0;
				   do
				   { 
				     /* generate the permutation */
				     {  int j;
				     /* generate the permutation of graph */
				     for (i = 0; i < N; i++)
				       {
				         permuted_graph.from[i] = a[graph.from[i]];
				         permuted_graph.to[i] = a[graph.to[i]];
				       }
				        
				     /* sort the graph */
				     for (i = 0; i < N; i++)
				       for (j = i; j < N; j++)
				         if ((permuted_graph.from[j] < permuted_graph.from[i]) ||
				             ((permuted_graph.from[j] == permuted_graph.from[i]) &&
				     		(permuted_graph.to[j] < permuted_graph.to[i])))
				         {
				           int tmp;
				           tmp = permuted_graph.to[j];
				           permuted_graph.to[j] = permuted_graph.to[i];
				           permuted_graph.to[i] = tmp;
				           tmp = permuted_graph.from[j];
				           permuted_graph.from[j] = permuted_graph.from[i];
				           permuted_graph.from[i] = tmp;
				         }
				     }
				     if (bsearch(&permuted_graph, graphs+prev_graphs,
				                 nbgraphs-prev_graphs,sizeof(struct edges),compar) != NULL)
				     {
				     found = 1;
				     break;
				     }
				     /* generate the permutation */
				     {  int j;
				     /* generate the permutation of graph */
				     for (i = 0; i < N; i++)
				       {                      
				         permuted_graph.from[i] = a[graph.to[i]];
				         permuted_graph.to[i] = a[graph.from[i]];
				       }
				     /* sort the graph */
				     for (i = 0; i < N; i++) 
				       for (j = i; j < N; j++)
				         if ((permuted_graph.from[j] < permuted_graph.from[i]) ||
				           ((permuted_graph.from[j] == permuted_graph.from[i]) &&     
				     	   (permuted_graph.to[j] < permuted_graph.to[i])))
				         {
				           int tmp;
				           tmp = permuted_graph.to[j];
				           permuted_graph.to[j] = permuted_graph.to[i];
				           permuted_graph.to[i] = tmp;
				           tmp = permuted_graph.from[j];
				           permuted_graph.from[j] = permuted_graph.from[i];
				           permuted_graph.from[i] = tmp;
				         }
				     }
				     if (bsearch(&permuted_graph, graphs+prev_graphs,
				                 nbgraphs-prev_graphs,sizeof(struct edges),compar) != NULL)
				     {                      
				     found = 1;
				     break;
				     }

				   } while(next_permutation(m-1) != -1);
				   
				   if (found)
				     ok = 0;
				 }
	  		if (ok)
	     		     {
			       graphs[nbgraphs++] = graph;
			       assert(nbgraphs < 1000000);    
			     }  
	  		}
	  	    j = N-1;
	  	    while ((j >= 0) && (graph.to[j] == m-1)) j--;
	  	    if (j == -1) break;
	  	    graph.to[j]++;
	  	    for (k = j+1; k < N; k++) graph.to[k] = 0;
	     }                                   
	  }
          if (part[1] < part[0] - 1)
          // H3
            { part[0]--; part[1]++; }
          else
            {
            // H4
            for (j = 2, s=part[0]+part[1]-1;(part[j] >= part[0]-1) && j < m; j++)
              s += part[j];
            // H5
            if (j >= m) break; 
            x = part[j]+1; part[j] = x; j--;
            // H6
            while (j > 0)
              { part[j] = x; s -= x; j--; }                         
            part[0] = s;
                  }                               
            }
  	  prev_graphs = nbgraphs;
     }
    { int  graph_fd = creat("graph.dat",  00755);
      int i;
      printf("number of graphs: %d\n", nbgraphs);
      for (i = 0; i < nbgraphs; i++)
      {
        write(graph_fd, graphs[i].from, N);
        write(graph_fd, graphs[i].to, N);
      }
      close(graph_fd);
    }
    return 0;
  }

  
