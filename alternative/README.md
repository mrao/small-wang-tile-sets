We describe here an alternative computer program that computes all sets of Wang tiles and tries to detect whether there are aperiodic or not.
It prove there is no aperiodic tileset with less than 10 Wang tiles. The program is written in C.

The program is written in \texttt{noweb}, a tool for literate programming, that permits to write the code and the documentation of the program at the same time.
We think it gives a good compromise between an unproven C program, and a time-consuming formal proof that our C program works.

To generate the tex file: noweave -delay nowang.nw > nowang.tex ; pdflatex nowang; pdflatex nowang

To generate the computer programs: noweb nowang.nw

To compile and generate the graph file (for N=7): gcc -O3 -DN=7 graph.c; ./a.out

To compile and generate the test programm (for N=7): gcc -O3 -DN=7 nowang.c; ./a.out

