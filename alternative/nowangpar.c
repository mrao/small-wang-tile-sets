  
  #include <sys/mman.h> /* for mmap */
  #include <stdio.h>    /* for printf */
  #include <sys/types.h>
  #include <sys/stat.h>
  #include <fcntl.h>
  #include <stdlib.h>

  #include <string.h>

#ifdef DEBUG
# define debug(x...) printf(x)
# define debug_tiles(x...) do { } while (0)
# define debug_times(x...) do { } while (0)
#else
# define debug(x...) do { } while (0)
# define debug_tiles(x...) do { } while (0)
# define debug_times(x...) do { } while (0)
#endif


  
  struct Tiles
  {
                  unsigned int south;     
                  unsigned int north;
                  unsigned int east;
                  unsigned int west;              
  };

  struct Tiles Wang[N];
  unsigned int nbgraphs;
  const unsigned char *n,*w;
  unsigned char* graph_buffer;

  unsigned int north_alt[N] = {0};
  unsigned int north_last[N] = {0};


  #define order(i,j)  ((Wang[i].west < Wang[j].west) || \
                       ((Wang[i].west == Wang[j].west && Wang[i].east < Wang[j].east)))
  #define swap(i,j)  {   unsigned int tmp = Wang[i].east; \
                         Wang[i].east = Wang[j].east;\
                         Wang[j].east = tmp;\
                         tmp = Wang[i].west; \
                         Wang[i].west = Wang[j].west;\
                         Wang[j].west = tmp;  }
  #define NOBJ N
  int next_permutation(int p)
  // Find the next permutation whose pth term is different from this one
  // return the first index that was changed
  {      
     int k,l,i,j;
     if (p < 0)
           return -1; 
     if (p >= NOBJ-1)
           {
                  
                  // Find the largest index k such that a[k] < a[k + 1].
                  for (k = NOBJ-2; k >= 0; k--)
                    if (order(k,k+1))
                          break;
                  if (k < 0)
                    // If no such index exists, the permutation is the last permutation.
                    return -1;
                  // Find the largest index l such that a[k] < a[l]. 
                  // Since k + 1 is such an index, l is well defined
                  // and satisfies k < l.
                  for (l = NOBJ-1; !order(k,l); l--);
                  // Swap a[k] with a[l].
                  swap(k,l);
                  // Reverse the sequence from a[k + 1] to a[n].
                  i = k + 1;
                  j = NOBJ - 1;
                  // note: the conditions i >= 0 && j >= 0 are unnecessary
                  // but are here for gcc to be happy
                  while ((i >= 0) && (j >= 0) && (i < j))
                    {
                           swap(i,j);
                           i++;
                           j--;
                    }
                  return k;
           }   
     else
           {
                  // find the smallest element of t[p+1:NOBJ-1] bigger than t[p] if it exists...
                  int i,j,smallest = -1;
                  for (i = p+1; i < NOBJ; i++)
                     if (order(p, i)) 
                        smallest = i;
                  if (smallest == -1)
                    /* not possible, we need to change something up high */
                    return next_permutation(p-1);
                  for (i = p+1; i < NOBJ; i++)
                    if (order(p, i))
                          if (order(i, smallest))                   
                            smallest = i;
                  swap(p,smallest);
                  // sort the remaining...
                  // this is insertion sort
                  for (j = p+1; j < NOBJ; j++)
                    {
                           for (i = j; i > p+1; i--)
                             {
                                if order(i-1,i)
                                          break;
                                    swap(i-1,i);
                             }
                    }
                  return p;
           }       
  }                                     
  #ifndef LIMIT
  #define LIMIT 2000009
  #endif

  #if LIMIT < 1000
  #error LIMIT is too small  
  #endif

  #define NEWSET(S) unsigned long long S##_set[16*LIMIT+16] = {0}; \
                    unsigned long long S##_level = 1;
  NEWSET(S1)
  NEWSET(S2)


  #define set_add(S,x)    S##_set[x] = S##_level
  #define set_member(S,x) (S##_set[x] == S##_level)
  #define set_clear(S)    ++S##_level


  struct Tiles T[LIMIT];
  struct Tiles U[LIMIT];
  struct Tiles V[LIMIT];
  unsigned int Ttiles = 0, Utiles, Vtiles;
  unsigned int renumber[16*LIMIT+16];

  int aperiodic()
  {
    int times;
    debug("The tileset we want to test:\n");
    debug_tiles(Wang, N);
    {
       struct Tiles* u;
       unsigned int i,j;
       u = U; 
       for (i = 0; i < N; ++i)
             {
                    unsigned int a = north_alt[Wang[i].south];
                    unsigned int b = north_last[Wang[i].south];
                    for (j= a; j < b; ++j)
                      {
                             u->north = Wang[i].north;
                             u->south = Wang[j].south;
                             u->east = Wang[i].east * 16 + Wang[j].east;
                             u->west = Wang[i].west * 16 + Wang[j].west;
                             u++;
                      }             
             }
       Utiles = (unsigned int) (u - U);
       debug("T2\n");
       debug_tiles(U, Utiles);

    }
      {
        struct Tiles*u;
        unsigned int i;
        unsigned int nt = 0;
        set_clear(S1); 
        for (i = 0, u = U; i < Utiles; i++, u++)
        {
              if (!set_member(S1,u->east))
              {
                set_add(S1,u->east);
                renumber[u->east] = ++nt;
              }
        }
      }
      {
        struct Tiles* v,*u;
        unsigned int i;
        v = V;
        for (i = 0, u = U; i < Utiles; i++,u++)
              {
                     if (set_member(S1,u->west))
                       {
                              v->west = renumber[u->west];
      						v->east = renumber[u->east];
                              v->north = u->north;
                              v->south = u->south;
                              v++;
                       }
              }
                Vtiles = (unsigned int) (v -V);
                debug("After renumbering\n");
                debug_tiles(V, Vtiles);
      }
                if (Vtiles < N)
                return 0;

      {
         unsigned int i;
         struct Tiles* v,*t;
         unsigned int previous= 0;   
         // The tiles are in V 
         while (previous != Vtiles)
         {
              previous = Vtiles;
              set_clear(S1);
              set_clear(S2);
              for (i = 0, v = V; i < Vtiles; v++,++i)
                {
                       set_add(S1, v->west);
                       set_add(S2, v->east);
                }
                      // S1 is the set of colors that appear on the west side
                      // S2 is the set of colors that appear on the east side
              t = T;
              for (i = 0, v = V; i < Vtiles; v++,++i)
                if (set_member(S1,v->east) && set_member(S2,v->west))
                      *t++ = *v;
              Ttiles = (unsigned int) (t - T);
              if (Ttiles < N)
                return 0;
                      // If we did not delete any tile, then all colors that appear
                      // on the east side also appear on the west side, and conversely.
                      // Also, T = U
              if (Ttiles == previous)
                break;
                      // This is exactly the same code as before, but with T instead of U               
              previous = Ttiles;
              set_clear(S1);
              set_clear(S2);
              for (i = 0, t = T; i < Ttiles; t++,++i)
                {
                       set_add(S1, t->west);
                       set_add(S2, t->east);
                }
              v = V;
              for (i = 0, t = T; i < Ttiles; t++,++i)
                if (set_member(S1,t->east) && set_member(S2,t->west))
                      *v++ = *t;
              Vtiles = (unsigned int) (v - V);
              if (Vtiles < N)
                return 0;
       }
      }

      {
          struct Tiles * u, *v;
          unsigned int i;
          u = U;
          for (i = 0, v = V; i < Vtiles; v++,++i)
                 if (v->north == v->south)
                   *u++ = *v;
          Utiles = (unsigned int) (u - U);
              debug("The test for periodicity starts from :\n");
              debug_tiles(U, Utiles);
      }
      if (Utiles != 0)
    {
       unsigned int i;
       struct Tiles* v,*u;
       unsigned int previous = 0;
       while (previous != Utiles)
     {      /* invariant: previous > 0 */
            if (Utiles == 0)
                      break;
            previous = Utiles;              
            set_clear(S1);
            set_clear(S2);
            for (i = 0, u = U; i < Utiles; u++,++i)
              {
                      set_add(S1, u->west);
                      set_add(S2, u->east);
              }
            v = V;
            for (i = 0, u = U; i < Utiles; u++,++i)
              if (set_member(S1,u->east) && set_member(S2,u->west))
                     *v++ = *u;
            Vtiles = (unsigned int) (v - V);
            if (Vtiles == previous)
              return 0;
            previous = Vtiles;
            set_clear(S1);
            set_clear(S2);
            for (i = 0, v = V; i < Vtiles; v++,++i)
              {
                      set_add(S1, v->west);
                      set_add(S2, v->east);
              }
            u = U;
            for (i = 0, v = V; i < Vtiles; v++,++i)
              if (set_member(S1,v->east) && set_member(S2,v->west))
                     *u++ = *v;
            Utiles = (unsigned int) (u - U);
    }
    }
    if (Utiles != 0)
    {
      debug("Periodicity succeedeed\n");
      return 0;
    } 

    for (times = 0; times < 100; times++)
    {
    debug_times(times);
    {
      unsigned int i;
      struct Tiles* u,*t;
      u = U; Utiles = 0;
      for (i = 0, t = T; i < Ttiles; i++,t++)
            {
                   unsigned int j;
                   unsigned int a = north_alt[t->south];
                   unsigned int b = north_last[t->south];
                   for (j= a; j < b; j++)
                     {
                            Utiles++;
                            if (Utiles > LIMIT)
                               return 2;
                            u->north = t->north;
                            u->south = Wang[j].south;
                            u->west = 16*t->west + Wang[j].west;
                            u->east = 16*t->east + Wang[j].east;
                            u++;
                     }
            }
       debug("T%d\n", times+3);
       debug_tiles(U,Utiles);
    }
      {
        struct Tiles*u;
        unsigned int i;
        unsigned int nt = 0;
        set_clear(S1); 
        for (i = 0, u = U; i < Utiles; i++, u++)
        {
              if (!set_member(S1,u->east))
              {
                set_add(S1,u->east);
                renumber[u->east] = ++nt;
              }
        }
      }
      {
        struct Tiles* v,*u;
        unsigned int i;
        v = V;
        for (i = 0, u = U; i < Utiles; i++,u++)
              {
                     if (set_member(S1,u->west))
                       {
                              v->west = renumber[u->west];
      						v->east = renumber[u->east];
                              v->north = u->north;
                              v->south = u->south;
                              v++;
                       }
              }
                Vtiles = (unsigned int) (v -V);
                debug("After renumbering\n");
                debug_tiles(V, Vtiles);
      }
                if (Vtiles < N)
                return 0;

      {
         unsigned int i;
         struct Tiles* v,*t;
         unsigned int previous= 0;   
         // The tiles are in V 
         while (previous != Vtiles)
         {
              previous = Vtiles;
              set_clear(S1);
              set_clear(S2);
              for (i = 0, v = V; i < Vtiles; v++,++i)
                {
                       set_add(S1, v->west);
                       set_add(S2, v->east);
                }
                      // S1 is the set of colors that appear on the west side
                      // S2 is the set of colors that appear on the east side
              t = T;
              for (i = 0, v = V; i < Vtiles; v++,++i)
                if (set_member(S1,v->east) && set_member(S2,v->west))
                      *t++ = *v;
              Ttiles = (unsigned int) (t - T);
              if (Ttiles < N)
                return 0;
                      // If we did not delete any tile, then all colors that appear
                      // on the east side also appear on the west side, and conversely.
                      // Also, T = U
              if (Ttiles == previous)
                break;
                      // This is exactly the same code as before, but with T instead of U               
              previous = Ttiles;
              set_clear(S1);
              set_clear(S2);
              for (i = 0, t = T; i < Ttiles; t++,++i)
                {
                       set_add(S1, t->west);
                       set_add(S2, t->east);
                }
              v = V;
              for (i = 0, t = T; i < Ttiles; t++,++i)
                if (set_member(S1,t->east) && set_member(S2,t->west))
                      *v++ = *t;
              Vtiles = (unsigned int) (v - V);
              if (Vtiles < N)
                return 0;
       }
      }

      {
          struct Tiles * u, *v;
          unsigned int i;
          u = U;
          for (i = 0, v = V; i < Vtiles; v++,++i)
                 if (v->north == v->south)
                   *u++ = *v;
          Utiles = (unsigned int) (u - U);
              debug("The test for periodicity starts from :\n");
              debug_tiles(U, Utiles);
      }
      if (Utiles != 0)
    {
       unsigned int i;
       struct Tiles* v,*u;
       unsigned int previous = 0;
       while (previous != Utiles)
     {      /* invariant: previous > 0 */
            if (Utiles == 0)
                      break;
            previous = Utiles;              
            set_clear(S1);
            set_clear(S2);
            for (i = 0, u = U; i < Utiles; u++,++i)
              {
                      set_add(S1, u->west);
                      set_add(S2, u->east);
              }
            v = V;
            for (i = 0, u = U; i < Utiles; u++,++i)
              if (set_member(S1,u->east) && set_member(S2,u->west))
                     *v++ = *u;
            Vtiles = (unsigned int) (v - V);
            if (Vtiles == previous)
              return 0;
            previous = Vtiles;
            set_clear(S1);
            set_clear(S2);
            for (i = 0, v = V; i < Vtiles; v++,++i)
              {
                      set_add(S1, v->west);
                      set_add(S2, v->east);
              }
            u = U;
            for (i = 0, v = V; i < Vtiles; v++,++i)
              if (set_member(S1,v->east) && set_member(S2,v->west))
                     *u++ = *v;
            Utiles = (unsigned int) (u - U);
    }
    }
    if (Utiles != 0)
    {
      debug("Periodicity succeedeed\n");
      return 0;
    } 

    }
    return 1;
  }
  int main(int argc, char **argv){
     {
     int fd;
     struct stat statb;
     fd = open("graph.dat", O_RDONLY);
     fstat(fd, &statb);
     nbgraphs= (unsigned int) statb.st_size;
     graph_buffer = (unsigned char*) mmap(0,(size_t) statb.st_size,
                                          PROT_READ,MAP_SHARED,fd,(off_t) 0);}

     n = graph_buffer+atoi(argv[1])*2*N;
     {
      unsigned int i,c = 0;
      printf("%Ld\n", (long long)(n - graph_buffer)/(2*N));
      fflush(stdout);
      for (i = 0; i < N; i++)
      {
        Wang[i].north = (*(n+i));
        Wang[i].south = (*(n+i+N));
       if (Wang[i].north == c)
        {
            north_alt[c] = i;
            if (c != 0) north_last[c-1] = i;
            c++;
        }
      }
      north_last[c-1] = N;
     }
     for (w = graph_buffer; w <= n; w+=2*N)
     {
        int i;

        for (i = 0; i < N; ++i){
           Wang[i].east = *(w+i);
           Wang[i].west = *(w+i+N);
        }
       { int pos = 0;
         while (pos != -1)
         {
       {
         int i;
         for (i = 0; i < N; ++i)
         if ((i >= 1)
             && (Wang[i].north == Wang[i-1].north
                 && Wang[i].south == Wang[i-1].south)
             && (Wang[i].west > Wang[i-1].west 
                 || (Wang[i].west == Wang[i-1].west
                     && Wang[i].east >= Wang[i-1].east)))
           break;
         if (i != N)
         {  pos = next_permutation(i);
                continue;
         }

       }
       {
         int k;
         for (k = 0; k < N; ++k)
                {
                      if (Wang[k].north == Wang[k].south)
                        {
                               if (Wang[k].east == Wang[k].west)
                                 break;
                        }
                }
         if (k != N)
                {
                      pos = next_permutation(k);
                      continue;
                }

       }
       {
         int i,j,tp = 0;
         for (i = 0; i < N; ++i)
         {
                if (Wang[i].north==Wang[i].south)
                  for (j = 0; j < i; ++j)
                        {
                               if ((Wang[j].north == Wang[j].south) &&
                                       (Wang[j].east == Wang[i].west) &&
                                       (Wang[i].east == Wang[j].west))
                                 {tp = 1; break;}
                        }
                if (tp) break;
         }
         if (tp)
         {
                pos = next_permutation(i);
                continue;
         }
       }
       {
         int i,tp = 0; 
         for (i = 0; i < N; ++i)
         {
                int l;
                if (Wang[i].east == Wang[i].west)
                for (l = 0; l < i; ++l)
                  if ((Wang[l].east == Wang[l].west) &&
                          (Wang[l].south == Wang[i].north) &&
                          (Wang[l].north == Wang[i].south)) 
                        { tp = 1; break; }
                if (tp) break;
         }
       if (tp)
         {
                pos = next_permutation(i);
                continue;
         }

       }
       { int j,l, tp = 0;

         for (j = 0; j < N; ++j)
         {
                for (l = 0; l < j; ++l)
                  if ((Wang[l].north == Wang[j].south) &&
                          (Wang[l].south == Wang[j].north) &&
                          (Wang[l].east == Wang[j].west) &&
                          (Wang[j].east == Wang[l].west))
                        { tp = 1; break; }
                if (tp) break;
         }
         if (tp)
         {
                pos = next_permutation(j);
                continue;
         }
       }
                   if (aperiodic() != 0)
	           {
	             int i;
	                         for (i = 0; i < N; i++)
	                           printf("%d%d%d%d", Wang[i].north, Wang[i].south, 
	                                   Wang[i].east, Wang[i].west);
	                         printf("\n");
	            fflush(stdout);              
	           }

         pos=next_permutation(N-1);
        }
       }
         }
    {
      munmap(graph_buffer, (size_t) nbgraphs);
    }


    printf("case %d done\n",atoi(argv[1]));
    return 0;
  }



