# Small Wang Tile Sets

A computer program to show that every Wang tile set with at most 10 tiles is not aperiodic.

This is the companion computer program of the paper "An aperiodic set of 11 Wang tiles" (Emmanuel Jeandel & Michael Rao) https://arxiv.org/abs/1506.06492


- "src" contains the source codes of the programs described in the paper.

- "results10" contains the hardest cases in the exploration of sets with at most 10 tiles.

- "results11" contains the sets of 11 tiles which are aperiodic, or for which the status is unknown.

- "alternative" contains an other, independent computer programm, which shows that there is no aperiodic Wang sets of at most 10 tiles.
