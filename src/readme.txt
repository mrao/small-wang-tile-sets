This folder contains the main source code.

- sets.hpp contains classes for sets of integers, using bit-operations. In addition to the usual operation (insertion, membership...) one have an operation "position" wich returns the position in the set. 

- graphs.hpp contains class for graphs with N edges (N is fixed, and is at most 10 in our case).

- wang.hpp contains class for set of Wang tiles. It contains functions to compute the power of two Wang set, some simplifications, and a check for periodicity.

- test.hpp test all bijections between the edges of two graphs, and then test is the resulting Wang set is not-aperiodic.

Two programs:

- test.cpp test all Wang sets for non-aperiodicity

- gengraphs.cpp generates all non-isomorphic graphs withs conditions of Lemma 4.

== Usage ==

Compilation (where N is an integer between 4 and 10):
$ make gengraphs_N
$ make test_N

Generation of the set of graphs :
$ ./gengraphs_N 

Test all Wang sets :
$ ./test_N 0 0 -1 -1

This takes several years for N=10...

You can split the research space :
$ ./test_N <a> <b> <a'> <b'>
Test all Wang sets for pairs of graph (i,j) with a<=i<a' and b<=j<b'.



