/*
  Function to check that a partition is a bi-simulation class.
  Note that we do not computes a bi-simultation equivalance class here.
*/

#include "wang.hpp"

// Helpers do deals with partitions
void populate(set<set<int> > &classes, const Wang_t &T)
{
  for(int i=0;i<T.size();i++) {
    int n=T[i].bfirst();
    set<int> s;
    s.insert(n);
    classes.insert(s);
  }
}

void removeset(set<set<int> > &classes, int n)
{
  set<int> s;
  s.insert(n);
  classes.erase(s);
}

void addset(set<set<int> > &classes, int n1,int n2)
{
  set<int> s;
  s.insert(n1);
  s.insert(n2);
  classes.insert(s);
}

string str(const set<set<int> > &classes) {
  string r="{";
  for(auto &it:classes) {
    if(r.size()>1) r=r+",";
    string s="{";
    for(auto &jt:it) {
      if(s.size()>1) s=s+",";
      char bf[100];
      snprintf(bf,99,"%d",jt);
      s=s+bf;
    }
    s+="}";
    r=r+s;
  }
  r+="}";
  return r;
}

int rep(const set<set<int> > &classes,int n)
{
  for(auto &it:classes) {
    if(it.find(n)!=it.end()) {
      return *(it.begin());
    }
  }
  assert(0);
}

// Check that the partition classes is a bisimuation equivalence for T.
bool isbisim(const Wang_t &T,const set<set<int> > &classes)
{
  for(auto &s:classes)
    for(auto &p:s)
      for(auto &q:s)
	if(p!=q)
	  for(int i=0;i<T.size();i++)
	    if(T[i].bfirst()==p) {
	      bool ok=0;
	      for(int j=0;j<T.size();j++)
		if(T[j].bfirst()==q && T[j].afirst()==T[i].afirst() && T[j].asecond()==T[i].asecond() && rep(classes,T[i].bsecond())==rep(classes,T[j].bsecond())) {
		  ok=1;
		}
	      if(!ok) return false;
	    }

  return true;
}

// Check that the partition classes is a co-bisimuation equivalence for T.
bool iscobisim(const Wang_t &T,const set<set<int> > &classes)
{
  for(auto &s:classes)
    for(auto &p:s)
      for(auto &q:s)
	if(p!=q)
	  for(int i=0;i<T.size();i++)
	    if(T[i].bsecond()==p) {
	      bool ok=0;
	      for(int j=0;j<T.size();j++)
		if(T[j].bsecond()==q && T[j].afirst()==T[i].afirst() && T[j].asecond()==T[i].asecond() && rep(classes,T[i].bfirst())==rep(classes,T[j].bfirst())) {
		  ok=1;
		}
	      if(!ok) return false;
	    }

  return true;
}

Wang_t contract(const Wang_t &T, const set<set<int> > &classes)
{
  Wang_t r;
  for(int i=0;i<T.size();i++)
    r.add(tile_t(T[i].afirst(),T[i].asecond(),rep(classes,T[i].bfirst()),rep(classes,T[i].bsecond())));
  r.remove_duplicate();
  return r;
}
