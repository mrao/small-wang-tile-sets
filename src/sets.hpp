/* 
   Classes for set of integers.

   In addition to the usual operation (insertion, membership...), one want an operation "position" to know the position in the set.

   Classes 'set64x_t<n>' (where n is a fixed integer) represent a set of integers between 0 and 64n-1.
   The class 'set_t' do the same job, without the bound limit (using std::map<>, and is slow but not heavily used).

   The class 'smallset_t' represent a set of integers between 0 and 31, with an operation "pick()" which return and remove the smallest element.

   If we know that the integers are bounded by a small constant, one can use bit operations to have operations with few machine instructions.
   We use _builtin_popcountll (which count the number of '1' in a word), and __builtin_ctz (which returns the number of '0' before the first '1'). 
   These operations can be done in one machine instruction on x86_64, if the program is compiled with '-mpopcnt'
*/

#ifndef _SETS_HPP_
#define _SETS_HPP_

#include "utils.hpp"

template<int n>
struct set64x_t {
  unsigned long long r[n];
  void swap(set64x_t &o) {
    for(int i=0;i<n;i++) {
      unsigned long long t=r[i];r[i]=o.r[i];o.r[i]=t;
    }
  }
  set64x_t() {for(int i=0;i<n;i++) r[i]=0;}
  void clear() {for(int i=0;i<n;i++) r[i]=0;}
  void insert(int i) {ASSERT(i>=0&&i<64*n);r[i/64]|=(1ULL<<(i%64));}
  bool present(int i) const {return (r[i/64]>>(i%64))&1;}
  int position(int i) const {
    int x=0;
    for(int j=0;j<i/64;j++)
      x+=__builtin_popcountll(r[j]);
    x+=__builtin_popcountll(r[i/64]&((1ULL<<(i%64))-1));
    return x;
  }
  int size() const {
    int x=0;
    for(int i=0;i<n;i++)
      x+=__builtin_popcountll(r[i]);
    return x;
  }
  bool operator==(const set64x_t &b) const {
    return memcmp(r,b.r,sizeof(r))==0;
  }
  bool operator!=(const set64x_t &b) const {
    return memcmp(r,b.r,sizeof(r))!=0;
  }
  void inter(const set64x_t &b) {for(int i=0;i<n;i++) r[i]&=b.r[i];}
};

/* specialization for n=1, since this class is heavily used */

template<>
struct set64x_t<1> {
  unsigned long long r;
  void swap(set64x_t &o) {
    unsigned long long t=r;r=o.r;o.r=t;
  }
  set64x_t():r(0) {}
  void clear() {r=0;}
  void insert(int i) {ASSERT(i>=0&&i<64);r|=(1ULL<<i);}
  bool present(int i) const {return (r>>i)&1;}
  int position(int i) const { return __builtin_popcountll(r&((1ULL<<i)-1));}
  int size() const {return __builtin_popcountll(r);}
  bool operator==(const set64x_t &b) const {
    return r==b.r;
  }
  bool operator!=(const set64x_t &b) const {
    return r!=b.r;
  }
  void inter(const set64x_t &b) {r&=b.r;}
};

using set64_t = set64x_t<1>;

struct set_t  {
  mutable map<int,int> m;
  mutable bool n=0;
public:
  size_t size() const {return m.size();}
  void swap(set_t &b) {
    m.swap(b.m);
    n=0;
  }
  void insert(int i) {n=0;m[i];}
  bool present(int i) const {return m.find(i)!=m.end();}
  void ATT_PROF compm() const {
    int k=0;
    n=1;
    for(auto &it:m)
      it.second=k++;
  }
  int position(int i) const { if(!n) compm(); return m.at(i);}
  void inter(const set_t &b) {
    map<int,int> rr;
    n=0;
    for(auto &it:m)
      if(b.present(it.first))
	rr[it.first];
    m.swap(rr);
  }
};

struct smallset_t {
  unsigned int s;
  smallset_t(int n):s((1<<n)-1){}
  smallset_t():s(0){}
  bool empty() const {return s==0;}
  smallset_t sub(int i) const {
    smallset_t r=*this;
    r.s&=~(1<<i);
    return r;
  }
  int pick() {
    int r=__builtin_ctz(s);
    s&=~(1<<r);
    return r;
  }
};

#endif
