/* 
   Classes for graphs with at most N edges (where N is fixed, and is less than 16)
*/

#ifndef _GRAPH_HPP_
#define _GRAPH_HPP_

#include <stdio.h>
#include <string.h>
#include <algorithm>
#include "sets.hpp"

/* One "edge", that is a pair of two integers. Since we have at most N<=16 vertices/colors in graphs, one can use only one byte */
class edge_t {
  unsigned char a;

public:
  edge_t(unsigned char b=0):a(b) {}
  edge_t(unsigned char b,unsigned char c):a((b<<4)|c) {}
  const bool operator<(const edge_t &o) const {return a<o.a;}
  const bool operator<=(const edge_t &o) const {return a<=o.a;}
  const bool operator>=(const edge_t &o) const {return a>=o.a;}
  const bool operator>(const edge_t &o) const {return a>o.a;}
  const bool operator==(const edge_t &o) const {return a==o.a;}
  bool loop() const {
    return 0==(((a>>4)^a)&0xF);
  }
  unsigned char rev() const 
  {
    return (a>>4)|(a<<4);
  }
  unsigned char first() const {
    return a>>4;
  }
  unsigned char second() const {
    return a&15;
  }
};

/* 
   One graph, that is N edges.
   If the graph is sorted, one can use an index: all edges starting from the vertex/color 'c' are in the interval [ ts[c] .. te[c] [
*/ 
class graph_t {
  edge_t edges[N];
  int ts[N],te[N];

public:

  void write(FILE *out) const {
    assert(sizeof(edges)==N);
    fwrite(edges,1,N,out);
  }
  
  edge_t &operator[](int i) {
    return edges[i];
  }

  const edge_t &operator[](int i) const {
    return edges[i];
  }

  bool operator<(const graph_t &b) const {
    for(int i=0;i<N;i++) {
      if(edges[i]<b.edges[i]) return 1;
      if(edges[i]>b.edges[i]) return 0;
    }
    return 0;
  }
  void print(int n=N) const {
    for(int i=0;i<n;i++) {
      printf("%d %d | ",int(edges[i].first()),int(edges[i].second()));
    }
    printf("\n");
  }

  bool sorted(int n=N) const {
    for(int i=0;i<n-1;i++)
      if(edges[i]>edges[i+1]) return 0;
    return 1;
  }
  
  void sort(int n=N) {
    std::sort(edges,edges+n);  
    assert(sorted(n));
  }

  
  void readgr(const unsigned char *w)
  {
    assert(sizeof(edges)==N);
    memcpy(edges,w,N);
    for(int i=0;i<N;i++)
      assert(edges[i].first()<8 && edges[i].second()<8);
  }

  void readstr(const char *w,int k=2,int a=0,int b=1)
  {
    for(int i=0;i<N;i++) {
      edge_t h(w[i*k+a]-'0',w[i*k+b]-'0');
      edges[i]=h;
    }
    for(int i=0;i<N;i++)
      assert(edges[i].first()<8 && edges[i].second()<8);
  }

  /* construct the index (the graph has to bo sorted) */
  void init_index() {
    assert(sorted());
    memset(ts,-1,sizeof(ts));
    memset(te,-1,sizeof(te));
    for(int i=0;i<N;i++) {
      int c=edges[i].first();
      if(ts[c]<0) ts[c]=i;
      te[c]=i+1;
    }
  }
  
  /* first edge starting from the vertex 'i' */
  const int &start(int i) const {
    return ts[i];
  }

  /* last edge +1 starting from the vertex 'i' */
  const int &end(int i) const {
    return te[i];
  }

  set64_t neightbor(const set64_t &a) const 
  {
    set64_t b;
    for(int j=0;j<N;j++) {
      if(a.present(edges[j].first()))
	b.insert(edges[j].second());
    }
    return b;
  }

  int num_colors(int n=N) const {
    int nc=0;
    for(int i=0;i<n;i++) {
      if(nc<=edges[i].first()) nc=edges[i].first()+1;
      if(nc<=edges[i].second()) nc=edges[i].second()+1;
    }
    return nc;
  }
};

/* Return the mirror graph of "g" */
graph_t reverse(const graph_t &g)
{
  graph_t r=g;
  for(int i=0;i<N;i++)
    r[i]=edge_t(r[i].second(),r[i].first());

  return r;
}

/* Returns a canonical representant of the graph 'g' (with 'n' edges).
   The alogirthm is naive and slow, but this function is only used to pre-compute the graphs.
*/
graph_t canonical(const graph_t &g,int n=N)
{
  int nc=g.num_colors(n);

  int perm_color[nc];
  for(int i=0;i<nc;i++)
    perm_color[i]=i;
  
  graph_t m=g;
  do {
    graph_t g2;
    for(int i=0;i<n;i++)
      g2[i]=edge_t(perm_color[g[i].first()],perm_color[g[i].second()]);
    for(int i=n;i<N;i++) {
      g2[i]=edge_t(0,0);
    }
    g2.sort(n);
    if(g2<m) {
      m=g2;
    }
  } while(next_permutation(perm_color,perm_color+nc));
  return m;
}

std::vector<graph_t> graphs;

void loadgraphs()   {
  char fn[20];
  snprintf(fn,19,"graphs%d.gr",N);
  FILE *in=fopen(fn,"r");
  assert(in);
  while(!feof(in)) {
    unsigned char x[N];
    int r=fread(x,1,N,in);
    if(r==0) break;
    assert(r==N);
    graph_t g;
    g.readgr(x);
    graphs.push_back(g);
  }
  fclose(in);
}

#endif
