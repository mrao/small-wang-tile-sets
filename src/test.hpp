/* This file contains the functions which test all Wang sets, given two graphs */

#include "wang.hpp"

/* The class 'proc_t' contains all the structures for one core computation 
   w1 and w2b are the original (sorded) graphs
   We generate all the permutations w2 of w2b, and then test the Wang set (w1,w2) for aperiodicity
 */

struct proc_t {
  int g1,g2; //the numbers of the graphs
  graph_t w1,w2,w2b; // graphs. w1 and w2b are the original graphs, and w2 is a permutation of w2b 

  Wang_t W,Wtr; // Wang sets for T^k or (T^t)^k
  Wang_t W_tmp,Wtr_tmp; //temporary Wang sets

  
  /* for statistics */
  long long rr,nap; 
  int maxpp,maxbp,maxbf; 
  int maxs1,maxs2;
  long long cnt[10]={0,0,0,0,0,0,0,0,0,0};
#define COUNT(a) {cnt[a+1]++;}

  proc_t():W(MAX),Wtr(MAX),W_tmp(MAX),Wtr_tmp(MAX) {  }

  
  /* Returns true if the tile k and a tile j<k tiles periodically */
  int per2(int k) { 
    edge_t a2=w2[k].rev(),a1=w1[k].rev();

    if (w1[k].loop())
      for (int j=0;j<k;j++)
	if (w1[j].loop() &&
	    w2[j]==a2)
	  return 1;

    if (w2[k].loop()) 
      for (int j=0;j<k;j++)
	if (w2[j].loop() &&
	    w1[j]==a1)
	  return 1;

    for (int j=0;j<k;j++)
      if(w2[j]==a2 && w1[j]==a1)
	return 1;

    return 0;
  }

  /* Test if the Wang tile set (w1,w2) is not aperiodic.
     If it returns 0, then the Wang set is not aperiodic.
     Otherwise it returns 1.

     We compute bands in the horizontal and vertical way on the same time.
     If SIMPL is defined, then we also remove duplicate tiles (it's slower, and not used except for ward wang set.
     */
  int test_aperiodic() {
    rr++;
    int r=W.t2_index(w1,w2);
    if(r>0) {
      COUNT(r);
      return 0;
    }
    r=Wtr.t2(w2,w1);
    if(r>0) {
      COUNT(r);
      return 0;
    }
    if(W.is_periodic() || Wtr.is_periodic()) {
      COUNT(NA_2_PER);
      return 0;
    }
    
    if(2>maxpp) maxpp=2;
    int pp;
    for(pp=3;pp<MAXW;pp++) {
      if(debug) {
	printf("%d %Ld %Ld\n",pp,W.size(),Wtr.size());
	fflush(stdout);
      }
#ifdef REMOVE_DUPLICATE
      W.remove_duplicate();
#endif
      r=W_tmp.tn(W,w1,w2);
      if(debug) {
	printf("1 r=%d %Ld\n",r,W_tmp.size());
	fflush(stdout);
      }
      if(r>0) {
	if(pp>maxbf) maxbf=pp;
	COUNT(r);
	return 0;
      }
      if(r<0) break;

      if(W_tmp.size()>maxs1) maxs1=W_tmp.size();
      if(W_tmp.is_periodic()) {
	if(pp>maxbp) maxbp=pp;
	COUNT(NA_N_PER);
	return 0;
      }
      W_tmp.swap(W);
      
#ifdef REMOVE_DUPLICATE
      Wtr.remove_duplicate();
#endif
      r=Wtr_tmp.tn(Wtr,w2,w1);
      if(debug) {
	printf("2 r=%d %Ld\n",r,Wtr_tmp.size());
	fflush(stdout);
      }
      if(r>0) {
	if(pp>maxbf) maxbf=pp;
	COUNT(r);
	return 0;
      }
      if(r<0) break;
      
      if(Wtr_tmp.size()>maxs2) maxs2=Wtr_tmp.size();
      if(Wtr_tmp.is_periodic()) {
	if(pp>maxbp) maxbp=pp;
	COUNT(NA_N_PER);
	return 0;
      }
      Wtr_tmp.swap(Wtr);
      if(pp>maxpp) maxpp=pp;
    }
    COUNT(0);
    printf("fail %d %Ld %Ld\n",pp,W.size(),Wtr.size());
    return 1;
  }

  
  /* A recursive function that generates all permutations w2 of w2b 
     'p' is the (partial) permutation, and 'i' an integer such that all 0<=j<i are set in 'p'
     'sr' is the set of integer [0..N] not already in p[0..i-1]
     If a partial permutation is already 1-periodic or 2-periodic, we cut the branch */
  void test_all_permutations(int i, int *p,smallset_t sr) {
    if(i==N) {
      if(test_aperiodic()) {
	/* if we are here, we have an hard case (a "candidate") */
	nap++;
	char fn[100];
	snprintf(fn,99,"hard%d.txt",int(N));
	FILE *out=fopen(fn,"a");
	assert(out);
	for(int i=0;i<N;i++) {
	  fprintf(out,"%d%d%d%d ",w1[i].first(),w1[i].second(),w2[i].first(),w2[i].second());
	}
	fprintf(out,"  %d %d\n",g1,g2);
	fclose(out);
      }
      return;
    }

    smallset_t s2 =sr;
    while(!s2.empty()) {
      int l=s2.pick();

      w2[i]=w2b[l];
      if(w1[i].loop() && w2[i].loop()) 
	continue; // the tile 'i' tiles periodically the plane 

      if(i>0 && w1[i]==w1[i-1] && w2[i-1]>=w2[i])
	continue; // this permutation is already tested
 
      p[i]=l;
      bool skip=0;
      for(int j=0;j<i;j++)
	if(w2[j]==w2[i] && p[j]>p[i]) {
	  skip=1; // this permutation is already tested
	  break;
	}

      if(skip || per2(i)) // test if the tile i with a pervious tile tile the plane (periodically)
	continue;

      test_all_permutations(i+1,p,sr.sub(l));
    }
  }

  void test_all_permutations() {
    rr=nap=0;
    maxpp=maxbp=maxbf=-1;
    maxs1=maxs2=0;
    int p[N];
    smallset_t s(N);
    test_all_permutations(0,p,s);
  }

  void init(int u,int v)
  {
    assert(u>=0 && u<graphs.size() && v>=0 && v<graphs.size());

    w1=graphs[u];
    w2b=graphs[v];
    
    assert(w1.sorted());
    assert(w2b.sorted());

    /*at most 8 colors... (otherwise, an overflow can occur)*/
    for(int i=0;i<N;i++) {
      assert(w1[i].first()<8);
      assert(w1[i].second()<8);
      assert(w2b[i].first()<8);
      assert(w2b[i].second()<8);
    }

    w1.init_index();
  }
};

/* Test if the Wang tile set T is not aperiodic.
   If it returns 0, then the Wang set is not aperiodic.
   Otherwise it returns 1.
   
   We compute bands ONLY in the horizontal way.
   It also remove duplicate tiles.
*/
int test_aperiodic_sc(const Wang_t &T) {
  Wang_t W(MAX),W_tmp(MAX);
  W=T;
  
  for(int pp=2;;pp++) {
    printf("pp=%d, size T_(%d)=%Ld\n",pp,pp-1,(long long)(W.size()));
    int r=W_tmp.tn_nosimpl(W,T);

    assert(r<=0); // tn_nosimpl() should never return that W*T is not aperiodic

    if(r<0) {
      printf("Overflow r=%d\n",r);
      return 1;
    }

    printf("%Ld -> ", (long long)(W_tmp.size())); fflush(stdout);
    W_tmp.remove_inter_sc();
    printf("%Ld -> ", (long long)(W_tmp.size())); fflush(stdout);
    W_tmp.remove_duplicate();
    printf("%Ld \n", (long long)(W_tmp.size()));

    if(W_tmp.size()==0) {
      printf("Finite: size=0\n");
      return 0;
    }

    if(W_tmp.is_periodic()) {
      printf("Periodic\n");
      return 0;
    }

    W_tmp.swap(W);
  }
}

