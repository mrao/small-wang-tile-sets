#define MAX 100000000 // max number of tiles
#define N 10

#include "wang.hpp"

/* 
   Here we test that the following Wang set does not tile the plane
   1112 1013 1001 0012 0123 0111 2231 2311 3331 3220
   
   On the "nodes" direction, 2 is the 2/3 node of Figure 2, and 3 is the 0' node
*/

/* the 4 tilesets found by the main program */
string hard[]={"0030 0100 0113 1002 1010 1130 2201 2300 3223 3301   21577 3322",
	       "0030 0100 0113 1002 1010 1130 2201 2323 3200 3301   21577 3322",
	       "0030 0102 0110 1000 1013 1130 2201 2300 3223 3301   21577 3322",
	       "0030 0102 0110 1000 1013 1130 2201 2323 3200 3301   21577 3322"};

void add(Wang_t &T,int a1,int a2,int b1,int b2) {
  T.add(tile_t(b2,b1,a1,a2));
}

int main() {
  lot_of_stack();

  //construct the tile set
  Wang_t T;
  add(T,1,1,1,2);
  add(T,1,0,1,3);
  add(T,1,0,0,1);
  add(T,0,0,1,2);
  add(T,0,1,2,3);
  add(T,0,1,1,1);
  add(T,2,2,3,1);
  add(T,2,3,1,1);
  add(T,3,3,3,1);
  add(T,3,2,2,0);

  /* show that the 4 tilesets found by the programm are isomorphic to T */
  {
    //canonican_with_mirror returns the smallest tileset among all permutations of tiles/colors  of T
    Wang_t W=canonical_with_mirror(T);
    W.print();
    for(int n=0;n<4;n++) {
      auto &str(hard[n]);
      Wang_t T2;
      for(int i=0;i<10;i++)
	add(T2,str[i*5]-'0',str[i*5+1]-'0',str[i*5+2]-'0',str[i*5+3]-'0');
      auto W2=canonical_with_mirror(T2);
      W2.print();
      assert(W==W2); // true iff T and T2 are isomorphic
    }
    printf("The 4 tilesets found by the program are isomorphic\n");
  }
  
  Wang_t W,W_tmp;

  // We use compactness here : if T tiles, then T tiles with an infinite line of '3'
  for(int i=0;i<T.size();i++) {
    if(T[i].afirst()==3)
      W.add(T[i]);
  }

  for(int pp=2;;pp++) {
    printf("pp=%d, size T_(%d)=%Ld\n",pp,pp-1,(long long)(W.size()));
    int r=W_tmp.tn_nosimpl(W,T);

    assert(r<=0); // tn_nosimpl() should never return that W*T is not aperiodic

    if(r<0) {
      printf("Overflow r=%d\n",r);
      break;
    }

    printf("%Ld -> ", (long long)(W_tmp.size())); fflush(stdout);
    W_tmp.remove_inter_sc();
    printf("%Ld -> ", (long long)(W_tmp.size())); fflush(stdout);
    W_tmp.remove_duplicate();
    printf("%Ld \n", (long long)(W_tmp.size()));

    if(W_tmp.size()==0) {
      printf("Finite: size=0\n");
      break;
    }

    if(W_tmp.is_periodic()) {
      printf("Periodic\n");
      break;
    }

    W_tmp.swap(W);
  }
  
  return 0;
}
