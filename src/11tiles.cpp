#define N 11

#include "wang.hpp"
#include "bisim.hpp"

#include <sys/stat.h>

// The 3 aperiodic tilesets in the paper:
const char *sets[]=
  {
   "3331 3111 3122 1142 1022 0010 0321 1323 3011 2214 2202 ",
   "3331 3111 3122 1102 1022 0010 0321 1323 3011 2210 2202 ",
   "3331 3111 3122 3342 1022 0010 0321 1323 3011 2214 2202 ",
  };
   
void add(Wang_t &T,int a1,int a2,int b1,int b2) {
  assert(a1>=0 && a1<=9);
  assert(a2>=0 && a2<=9);
  assert(b1>=0 && b1<=9);
  assert(b2>=0 && b2<=9);
  T.add(tile_t(b1,b2,a1,a2));
}

// Computes T_s
// If basic=1, we do not relabel colors
Wang_t pow(const Wang_t T[2],const string &s,bool basic=0)
{
  Wang_t W=T[s[0]-'0'],W_tmp;
  for(int i=1;i<s.size();i++) {
    int r;
    if(!basic)
      r=W_tmp.tn_nosimpl(W,T[s[i]-'0']);
    else
      r=W_tmp.tn_basic(W,T[s[i]-'0']);
    assert(r==0);
    W_tmp.remove_duplicate();
    W_tmp.remove_inter_sc();
    W_tmp.swap(W);
  }
  return W;
}

// Recursively computes all binary words starting with 's', ending with '010', anf without factors in 'forb'
void rec010(const string &s, const set<string> &forb) {
  for(auto &it:forb)
    if(strstr(s.c_str(),it.c_str())) return;
  if(strstr(s.c_str()+1,"010")) {
    printf("%s\n",s.c_str());
    return;
  }
  rec010(s+"0",forb);
  rec010(s+"1",forb);
}

// 1) check that T does not read either 2 or 3
// 2) remove the transitions that writes 2 or 3
//    then remove transitions between two SC components
Wang_t no23(const Wang_t &T)
{
  for(int i=0;i<T.size();i++)
    assert(T[i].afirst()<2);
  Wang_t Q;

  for(int i=0;i<T.size();i++)
    if(T[i].asecond()<2)
      Q.add(T[i]);

  Q.remove_inter_sc();
  return Q;
}

Wang_t T[3]; // we will store were the 3 aperiodic sets
Wang_t TT[3][2]; // we will store T_0 and T_1 (for the 3 sets)

// Generate the figure for a tileset, with "square" Wang tiles
void printsquares(const char *fn,const Wang_t &w)
{
  FILE *out=fopen(fn,"w");
  assert(out);
  fprintf(out,"\\begin{tikzpicture}\n");
  for(int i=0;i<w.size();i++) {
    int x=i%6;
    int y=i/6;
    double xx=x*1.3;
    double yy=y*1.3;
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx,yy,xx+1,yy);
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx,yy,xx+1,yy+1);
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx,yy,xx,yy+1);
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx+1,yy,xx+1,yy+1);
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx,yy+1,xx+1,yy+1);
    fprintf(out,"\\draw (%lf,%lf) -- (%lf,%lf) ;\n",xx,yy+1,xx+1,yy);
    fprintf(out,"\\draw (%lf,%lf) node[left]{$%d$} ;\n",xx+0.42,yy+.5,w[i].bfirst());
    fprintf(out,"\\draw (%lf,%lf) node[right]{$%d$} ;\n",xx+0.57,yy+.5,w[i].bsecond());
    fprintf(out,"\\draw (%lf,%lf) node[above]{$%d$} ;\n",xx+0.5,yy+.54,w[i].asecond());
    fprintf(out,"\\draw (%lf,%lf) node[below]{$%d$} ;\n",xx+0.5,yy+.46,w[i].afirst());
  }
  fprintf(out,"\\end{tikzpicture}\n");
  fclose(out);
}

// structure to generate the figure for a transducer 
struct tikz_t { 
  map<int,string> pos_nodes; //helper for positions
  map<int,string> names; //names
  map<tile_t,string> pos_edges; //helper for edges

  void clear() {
    pos_nodes.clear();
    names.clear();
    pos_edges.clear();
  }

  
  void readpos(const char *fn) { //read an file for the helpers
    FILE *in=fopen(fn,"r");
    if(!in) return;
    char bf[1000];
    while(fgets(bf,999,in)) {
      if(strncmp(bf,"\\node[state] ",13)==0) {
	int d=atoi(bf+15);
	char *p=strstr(bf," at ");
	assert(p);
	p+=4;
	p=strtok(p," ");
	pos_nodes[d]=p;
	p=strtok(NULL," {}\n");
	if(p)
	  names[d]=p;
	continue;
      }
      if(strncmp(bf,"\\path[trans] ",13)==0) {
	int b1=atoi(bf+15);
	char *p=strstr(bf,"edge");
	assert(p);
	p=strtok(p,"{");
	string pos=p;
	p=strtok(NULL,"{$|$} (n);");
	int a1=atoi(p);
	p=strtok(NULL,"{$|$} (n);");
	int a2=atoi(p);
	p=strtok(NULL,"{$|$} (n);");
	int b2=atoi(p);
	pos_edges[tile_t(a1,a2,b1,b2)]=pos;
	continue;
      }
    }
    
    fclose(in);
  }

  void write(FILE *out,const Wang_t &w) const { // writes the figure
    set<int> nodes;
    for(int i=0;i<w.size();i++) {
      nodes.insert(w[i].bfirst());
      nodes.insert(w[i].bsecond());
    }
    for(auto &it:nodes) {
      if(pos_nodes.find(it)!=pos_nodes.end())
	if(names.find(it)!=names.end())
	  fprintf(out,"\\node[state] (n%d) at %s {%s};\n",it,pos_nodes.at(it).c_str(),names.at(it).c_str());
	else
	  fprintf(out,"\\node[state] (n%d) at %s {%d};\n",it,pos_nodes.at(it).c_str(),it);
      else 
	fprintf(out,"\\node[state] (n%d) at (%d,%d) {%d};\n",it,rand()%10,rand()%10,it);
    }
    for(int i=0;i<w.size();i++) {
      if(pos_edges.find(w[i])!=pos_edges.end())
	fprintf(out,"\\path[trans] (n%d) %s {$%d|%d$} (n%d);\n",w[i].bfirst(),pos_edges.at(w[i]).c_str(),w[i].afirst(),w[i].asecond(),w[i].bsecond());
      else
	fprintf(out,"\\path[trans] (n%d) edge node[tr] {$%d|%d$} (n%d);\n",w[i].bfirst(),w[i].afirst(),w[i].asecond(),w[i].bsecond());
    }
  }

  void write(const char *fn,const Wang_t &w) const { //write the figure
    FILE *out=fopen(fn,"w");
    assert(out);
    write(out,w);
    fclose(out);
  }
    
};

int main(int ac, char **av) {
  mkdir("11figs",0755);

  for(int num=0;num<3;num++) {
    for(int i=0;i<9;i++)
      add(TT[num][0],sets[num][i*5]-'0',sets[num][i*5+1]-'0',sets[num][i*5+2]-'0',sets[num][i*5+3]-'0');
    for(int i=9;i<11;i++)
      add(TT[num][1],sets[num][i*5]-'0',sets[num][i*5+1]-'0',sets[num][i*5+2]-'0',sets[num][i*5+3]-'0');

    TT[num][0].sort();
    
    T[num].add(TT[num][0]);
    T[num].add(TT[num][1]);

#if 1
    Wang_t W=canonical_with_mirror(T[num]);
    printf("canonical form of aperiodic set number %d:\n",num);
    W.print();
#endif
  }

  printsquares("11figs/0_w11.tex",T[0]);
  printsquares("11figs/1_w11.tex",T[1]);
  printsquares("11figs/2_w11.tex",T[2]);

  
  Wang_t W;

  printf("First aperiodic set\n");

  
  printf("Proof of Fact 4:\n"
	 "The transducers $s{T_{11}}$, $s{T_{101}}$, $s{T_{1001}}$ and $s{T_{00000}}$ are empty.\n");
  
  W=pow(TT[0],"11");
  assert(W.size()==0);
  W=pow(TT[0],"101");
  assert(W.size()==0);
  W=pow(TT[0],"1001");
  assert(W.size()==0);
  W=pow(TT[0],"00000");
  assert(W.size()==0);

  printf("Proof of Fact 4 OK\n");

  Wang_t T0a[2];
  T0a[0]=pow(TT[0],"10000",1);
  T0a[1]=pow(TT[0],"1000",1);

  tikz_t tikz;
  tikz.readpos("11figs/0_10000.hint");
  tikz.write("11figs/w10000z.tex",T0a[0]);
  tikz.readpos("11figs/0_1000.hint");
  tikz.write("11figs/w1000z.tex",T0a[1]);
  
  Wang_t T0b[2];
  T0b[0]=no23(T0a[0]);
  T0b[1]=no23(T0a[1]);

  tikz.readpos("11figs/0_10000.hint");
  tikz.write("11figs/w10000_2z.tex",T0b[0]);
  tikz.readpos("11figs/0_1000.hint");
  tikz.write("11figs/w1000_2z.tex",T0b[1]);

  // we construct a partitions of sets
  set<set<int> > classes;
  populate(classes,T0b[0]);
  removeset(classes,23300);
  removeset(classes,23310);
  removeset(classes,21300);
  removeset(classes,21310);
  addset(classes,23300,23310);
  addset(classes,21300,21310);
  assert(iscobisim(T0b[0],classes));

  printf("The partition %s is a co-bisimulation equivalence of T0b[0]\n",str(classes).c_str());

  T0b[0]=contract(T0b[0],classes);
  
  classes.clear();
  populate(classes,T0b[1]);
  removeset(classes,2300);
  removeset(classes,2310);
  addset(classes,2300,2310);
  assert(iscobisim(T0b[1],classes));

  printf("The partition %s is a co-bisimulation equivalence of T0b[1]\n",str(classes).c_str());

  T0b[1]=contract(T0b[1],classes);

  tikz.readpos("11figs/0_10000_b.hint");
  tikz.write("11figs/w10000_3z.tex",T0b[0]);
  tikz.readpos("11figs/0_1000_b.hint");
  tikz.write("11figs/w1000_3z.tex",T0b[1]);

  // no 101
  {
    Wang_t Q=transpose(T[0]);
    //TODO
  }
  
  printf("\nSecond aperiodic set\n");
  
  printf("Proof of Fact 6:\n"
	 "The transducers $s{T'_{111}}$, $s{T'_{101}}$, $s{T'_{1001}}$, $s{T'_{1000001}}$, $s{T'_{10000001}}$, $s{T'_{100000001}}$, $s{T'_{000000000}}$, $s{T'_{000011}}$, $s{T'_{110000}}$ and $s{T'_{1100011}}$ are empty\n");

  set<string> forb;
  W=pow(TT[1],"111");forb.insert("111");
  assert(W.size()==0);
  W=pow(TT[1],"101");forb.insert("101");
  assert(W.size()==0);
  W=pow(TT[1],"1001");forb.insert("1001");
  assert(W.size()==0);
  W=pow(TT[1],"1000001");forb.insert("1000001");
  assert(W.size()==0);
  W=pow(TT[1],"10000001");forb.insert("10000001");
  assert(W.size()==0);
  W=pow(TT[1],"100000001");forb.insert("100000001");
  assert(W.size()==0);
  W=pow(TT[1],"000000000");forb.insert("000000000");
  assert(W.size()==0);
  W=pow(TT[1],"000011");forb.insert("000011");
  assert(W.size()==0);
  W=pow(TT[1],"110000");forb.insert("110000");
  assert(W.size()==0);
  W=pow(TT[1],"1100011");forb.insert("1100011");
  assert(W.size()==0);
  printf("Proof of Fact 6 OK\n\n");

  printf("Now we show that an bi-infinite binary word without words in 'forb' is a concatenation of 1000, 10000, 100011000 and 100000000.\n");

  printf("Such a word must contains '11', and thus must contains '010'\n");

  printf("If we cut a 'forb'-free bi-infinite word at each position between 0 and 10 in 010, we get the following 4 possibles words\n");

  printf("List of possible binary words starting with 010 and ending with 010:\n");

  rec010("010",forb);

  printf("\nWe show now that :\n"
	 "$T'_{11}$ is isomorphic to a subset of $T'_{01}$, and $T'_{100000}$ is isomorphic to a subset of $T'_{100001}$\n");
    
  printf("\nWe verify that each tile of T'_11 (resp T'_100000) is a tile of T'_01 (resp T'_100001), after a simple relabeling of the colors:\n");

  {
    auto A=pow(TT[1],"11",1);
    auto B=pow(TT[1],"01",1);
    for(int i=0;i<A.size();i++) {
      bool found=0;
      auto t=tile_t(A[i].afirst(),A[i].asecond(),A[i].bfirst()-20,A[i].bsecond()-20);
      for(int j=0;j<B.size();j++)
	if(B[j]==t) found=1;
      assert(found);
    }
  }
  {
    auto A=pow(TT[1],"100000",1);
    auto B=pow(TT[1],"100001",1);
    for(int i=0;i<A.size();i++) {
      bool found=0;
      auto t=tile_t(A[i].afirst(),A[i].asecond(),A[i].bfirst()+1,A[i].bsecond()+1);
      for(int j=0;j<B.size();j++)
	if(B[j]==t) found=1;
      assert(found);
    }
  }
  printf("OK\n");
  

  printf("\nLast aperiodic Wang set\n");
  
  W=pow(TT[2],"11");
  assert(W.size()==0);
  W=pow(TT[2],"101");
  assert(W.size()==0);
  W=pow(TT[2],"1001");
  assert(W.size()==0);
  W=pow(TT[2],"00000");
  assert(W.size()==0);

  Wang_t T2a[2];
  T2a[0]=pow(TT[2],"10000",1);
  T2a[1]=pow(TT[2],"1000",1);

  Wang_t T2b[2];
  T2b[0]=no23(T2a[0]);
  T2b[1]=no23(T2a[1]);
  
  Wang_t shift; 
  shift.add(tile_t(0,0,0,0));
  shift.add(tile_t(1,0,1,0));
  shift.add(tile_t(0,1,0,1));
  shift.add(tile_t(1,1,1,1));
  
  tikz.readpos("11figs/shift.hint");
  tikz.write("11figs/shift.tex",shift);
  
  //we shift the transducers
  Wang_t T2bs[2];
  T2bs[0].tn_basic(T2b[0],shift);
  T2bs[0].remove_inter_sc();
  T2bs[0].remove_duplicate();
  T2bs[1].tn_basic(T2b[1],shift);
  T2bs[1].remove_inter_sc();
  T2bs[1].remove_duplicate();

  tikz.readpos("11figs/2_w10000z.hint");
  tikz.write("11figs/2_w10000z.tex",T2bs[0]);

  tikz.readpos("11figs/2_w1000z.hint");
  tikz.write("11figs/2_w1000z.tex",T2bs[1]);

  classes.clear();
  populate(classes,T2bs[0]);
  removeset(classes,231000);
  removeset(classes,231031);
  addset(classes,231000,231031);
  removeset(classes,230300);
  removeset(classes,230331);
  addset(classes,230300,230331);

  assert(isbisim(T2bs[0],classes));

  printf("The partition %s is a bisimulation equivalence of T2b[0]\n",str(classes).c_str());

  T2bs[0]=contract(T2bs[0],classes);

  classes.clear();
  populate(classes,T2bs[1]);
  removeset(classes,21331);
  removeset(classes,21300);
  addset(classes,21331,21300);
  removeset(classes,20331);
  removeset(classes,20300);
  addset(classes,20331,20300);
  removeset(classes,23000);
  removeset(classes,23031);
  addset(classes,23000,23031);
  assert(isbisim(T2bs[1],classes));

  printf("The partition %s is a bisimulation equivalence of T2b[1]\n",str(classes).c_str());

  T2bs[1]=contract(T2bs[1],classes);

  tikz.readpos("11figs/2_w10000z_3.tex");
  tikz.write("11figs/2_w10000z_3.tex",T2bs[0]);

  tikz.readpos("11figs/2_w1000z_3.hint");
  tikz.write("11figs/2_w1000z_3.tex",T2bs[1]);
  
  //We check that the two transducers we got are the two transducers in T_C in Figure 7.

  //We give the bijection 'm'
  map<unsigned int,unsigned int> m;
  m[233000]=21130;
  m[211300]=20330;
  m[203300]=23100;
  m[231000]=21030;
  m[231300]=21330;
  m[233301]=21113;
  m[231131]=21033;
  m[230300]=21300;
  m[213300]=23300;
  m[233101]=21103;

  m[23131]=2133;
  m[23301]=2113;
  m[21131]=2033;
  m[20300]=2300;
  m[21031]=2030;
  m[21300]=2330;
  m[23000]=2130;
  m[23101]=2103;

  
  T2bs[0].remap(m);
  T2bs[0].sort();
  assert(T2bs[0]==T0b[0]);

  T2bs[1].remap(m);
  T2bs[1].sort();
  assert(T2bs[1]==T0b[1]);

  printf("\nEverything is OK !\n");
  return 0;
  
}
