#ifdef VLARGE
#define MAX 100000000
#define MAXP 10000
#else
#define MAX 1000000
#define MAXP 10000
#endif

//#define DEBUG

#include "test.hpp"

proc_t pr;
int main(int ac, char **av) {
  loadgraphs();

  int nb=graphs.size();
  cout<<"N="<<N<<" #nbgraphs="<<nb<<endl;
  
  long long nap=0;
  long long tta=0,ttb=0;
  
  int ust=atoi(av[1]);
  int vst=atoi(av[2]);
  int uen=ust+1;
  if(ac>3) uen=atoi(av[3]);
  int ven=vst+1;
  if(ac>4) ven=atoi(av[4]);
  if(uen<0) uen=nb;
  if(ven<0) ven=nb;
  for(int u=ust;u<uen;u++)
    for(int v=vst;v<ven;v++) {
      pr.g1=u;
      pr.g2=v;

      auto start=PROF();
      pr.init(u,v);
      pr.test_all_permutations();
      auto end=PROF();
      cout<<u<<" "<<v<<" : "<<endl;
      cout<<"#candidates aperiodic: "<<pr.nap<<endl;
      cout<<"max width: "<<pr.maxpp<<"  ";
      cout<<"(max widhts periodic/finite: "<<pr.maxbp<<" / "<<pr.maxbf<<") ";
      cout<<"max sizes: "<<pr.maxs1<<" / "<<pr.maxs2<<endl;
      cout<<"#test: "<<pr.rr<<"  ";
      cout<<"time: "<<(end-start)/1000/1000<<" ms\n";
    }

  return 0;
}
