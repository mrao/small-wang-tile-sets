/*
  This program generates the graphs withs contitions of Lemma 4

  N : number of graphs
  -------------------
  4 : 6
  5 : 26
  6 : 122
  7 : 516
  8 : 2517
  9 : 13276
  10: 77809
  
  These are the md5sums you should get:

  786c7b8830028720fe617708cafbca8c  graphs4.gr
  aa3e8a316a9e70243e715fc15043c81f  graphs5.gr
  72e9bfc61834406dfc307fbde466fdb7  graphs6.gr
  fb6907472b52d1135cbbc3da45e930e9  graphs7.gr
  6b889ee144dec32aa58df353623afb99  graphs8.gr
  6f46016cc32f3e67b8262113104e86c0  graphs9.gr
  6d46e56f5c48865c9056709a711adfe3  graphs10.gr

*/

#include "graph.hpp"

/* Test conditions of Lemma 4 */
bool test_conditions(const graph_t &g) {

  /* First, a quick test */
  {
    set64_t a,b;
    for(int i=0;i<N;i++) {
      a.insert(g[i].first());
      b.insert(g[i].second());
    }
    if(!(a==b)) return 0; // there is a source or a sink
    if(a.size()==1) return 0; // only one vertex/color
  }

  set64_t s[N];
  for(int i=0;i<N;i++) {
    set64_t a,b;
    a.insert(i);
    b=g.neightbor(a);
    if(b.size()>0) {
      b.insert(i);
      while(a!=b) {
	a=b;
	b=g.neightbor(a);
	b.insert(i);
      }
      if(g.neightbor(a)!=a) {
	return 0; // vertex 'i' is not in a SC component (and is not an isolated vertex)
      }
      s[i]=a;
    }
  }
  
  for(int i=0;i<N;i++)
    for(int j=0;j<N;j++)
      if(s[i].present(j) && ! s[j].present(i)) return 0; //there is an edge (i,j) between two SC components

  for(int i=0;i<N;i++)
    if(s[i].size()>0) {
      int ne=0;
      for(int j=0;j<N;j++)
	if(s[i].present(g[j].first())) ne++;
      assert(ne>=s[i].size());
      if(ne==s[i].size()) return 0; // one SC comp is a cycle
    }
  
  return 1;
}

/* 
  Try to add a n-th edge (a,b) to the graph g. We requied wlog that after the adding, the out-degree of a is the maximal out-degree of the graph.
  If this true, and the graph is not isomortphic to a graph already in the set 's', we add it to 's'
 */
void tryadd(const graph_t &g,int n,int a,int b,set<graph_t> &s)
{
  auto g2=g;
  g2[n-1]=edge_t(a,b);
  
  int deg[N];
  memset(deg,0,sizeof(deg));
  for(int i=0;i<n;i++)
    deg[g2[i].first()]++;
  for(int i=0;i<n;i++)
    if(deg[g2[i].first()]>deg[g2[n-1].first()]) return;

  /* If we add the last (N-th) edge, we also test the conditions of Lemma 4*/
  if(n==N && !test_conditions(g2)) return; 

  s.insert(canonical(g2,n));
}

int main(int ac, char **av) {
  set<graph_t> l;

  graph_t g0;
  for(int i=0;i<N;i++)
    g0[i]=edge_t(0,0);
  l.insert(g0);

  for(int n=1;n<=N;n++) {
    set<graph_t> s;
    for(auto &it:l) {
      int nc=it.num_colors(n-1);

      for(int a=0;a<nc;a++) // don't add new vertex
	for(int b=0;b<nc;b++)
	  tryadd(it,n,a,b,s);

      if(nc<N-2) { // add one new vertex
	tryadd(it,n,nc,nc,s);
	for(int a=0;a<nc;a++) {
	  tryadd(it,n,nc,a,s);
	  tryadd(it,n,a,nc,s);
	}
      }

      if(nc<N-3) // add two new vertices
	tryadd(it,n,nc,nc+1,s);
    }
    l.swap(s);
    cout<<n<<" "<<l.size()<<endl;
  }

  /* keeps only one representant for G and the mirror of G */
  {
    set<graph_t> s;

    for(auto &it:l) {
      graph_t r=canonical(reverse(it));
      if(r<it)
	continue;
      s.insert(it);
    }
    l.swap(s);
  }
  
  /* saves the list of graphs */
  cout<<l.size()<<endl;
  char fn[20];
  snprintf(fn,19,"graphs%d.gr",int(N));
  FILE *out=fopen(fn,"w");
  for(auto &it:l)
    it.write(out);
  fclose(out);
  

  return 0;
}
