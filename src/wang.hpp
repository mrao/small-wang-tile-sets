#ifndef _WANG_HPP_
#define _WANG_HPP_

#include <array>
#include <vector>
#include <algorithm>
using namespace std;

#include "utils.hpp"
#include "sets.hpp"
#include "graph.hpp"

#if 0
/* One wang tile, that is 4 integers 
   afirst/asecond are colors in the directions north/south
   bfirst/bsecond are colors in the directions west/east
*/
class tile_t : public array<unsigned int,4>{
public:
  const unsigned int &afirst() const {return (*this)[0];}
  const unsigned int &asecond() const {return(*this)[1];}
  const unsigned int &bfirst() const {return (*this)[2];}
  const unsigned int &bsecond() const {return (*this)[3];}
  unsigned int &bfirst() {return (*this)[2];}
  unsigned int &bsecond() {return (*this)[3];}
  tile_t() {memset(this,0,sizeof(tile_t));}
  tile_t(unsigned int a1,unsigned int a2,unsigned int b1,unsigned int b2) {
    array<unsigned int,4> &t(*this);
    t[0]=a1;
    t[1]=a2;
    t[2]=b1;
    t[3]=b2;
  }
};

#else

/* One wang tile, that is 4 integers 
   afirst/asecond are colors in the directions north/south
   bfirst/bsecond are colors in the directions west/east
*/
class tile_t {
  array<unsigned int,4> t;
public:
  const unsigned int &afirst() const {return t[0^2];}
  const unsigned int &asecond() const {return t[1^2];}
  const unsigned int &bfirst() const {return t[2^2];}
  const unsigned int &bsecond() const {return t[3^2];}
  unsigned int &bfirst() {return t[2^2];}
  unsigned int &bsecond() {return t[3^2];}
  tile_t() {memset(&t,0,sizeof(tile_t));}
  unsigned int &operator[](int i) { return t[i^2];}
  unsigned int operator[](int i) const { return t[i^2];}
  bool operator<(const tile_t &b) const {
    return t<b.t;
  }
  bool operator==(const tile_t &b) const {
    return t==b.t;
  }
  tile_t(unsigned int a1,unsigned int a2,unsigned int b1,unsigned int b2) {
    t[0^2]=a1;
    t[1^2]=a2;
    t[2^2]=b1;
    t[3^2]=b2;
  }
};
#endif

/* return codes */
#define NA_2_SMALL 1 // the tile set has less than N tiles (and thus is not aperiodic, since every tile set with less than N tiles are already tested)
#define NA_N_SMALL 2

#define NA_2_PER_1 5 // the tile set is periodic
#define NA_N_PER_1 6
#define NA_2_PER 7
#define NA_N_PER 8

#define OVERFLOW -1 //the tile set is too big

#ifndef MAX
#define MAX 1000000 // max number of tiles
#endif
#ifndef MAXW
#define MAXW 10000 // max width of one band
#endif

/* A set of 'n' Wang tiles 
   'mc' is one plus the maximum number of a vertex/color in the west/east directions (bfirst/bsecond)
   For performances reasons, we pre-allocate the vector of tiles at the begining (default:MAX tiles), and work in place.
*/

class Wang_t {
  long long n;
  int mc;
  vector<tile_t> W;

public:

  Wang_t(long long sizemax=MAX):n(0),mc(0),W(sizemax) {}

  void clear() {n=0;mc=0;}
  
  const tile_t &operator[](int i) const {return W[i];}
  
  long long size() const { return n;}

  const Wang_t &operator=(const Wang_t &b)
  {
    if(this!=&b) {
      n=b.n;
      mc=b.mc;
      assert(n<W.size());
      for(long long i=0;i<n;i++)
	W[i]=b.W[i];
    }
    return *this;
  }
  
  bool operator==(const Wang_t &b) const {
    if(n!=b.n) return false;
    for(int i=0;i<n;i++)
      if(!(W[i]==b.W[i])) return false;
    return true;
  }

  bool operator<(const Wang_t &b) const {
    if(n!=b.n) return n<b.n;
    for(int i=0;i<n;i++)
      if(!(W[i]==b.W[i])) return W[i]<b.W[i];
    return false;
  }

  void swap(Wang_t &b) {
    int tmp=n;n=b.n;b.n=tmp;
    tmp=mc;mc=b.mc;b.mc=tmp;
    W.swap(b.W);
  }

  void print() const {
    for(int i=0;i<n;i++)
      printf("%d %d %d %d | ",int(W[i].afirst()),int(W[i].asecond()),int(W[i].bfirst()),int(W[i].bsecond()) );
    printf(" n=%Ld mc=%d\n",n,mc);
  }

  
  /* Returns the number of colors in e/w direction */
  int num_colors_a() const {
    int nc=0;
    for(int i=0;i<n;i++) {
      if(nc<=W[i].afirst()) nc=W[i].afirst()+1;
      if(nc<=W[i].asecond()) nc=W[i].asecond()+1;
    }
    return nc;
  }

  /* Returns the number of colors in n/s direction */
  int num_colors_b() const {
    int nc=0;
    for(int i=0;i<n;i++) {
      if(nc<=W[i].bfirst()) nc=W[i].bfirst()+1;
      if(nc<=W[i].bsecond()) nc=W[i].bsecond()+1;
    }
    return nc;
  }

  /* Add a new tile */
  void add(const tile_t &t) {
    if(mc<=t.bfirst()) mc=1+t.bfirst();
    if(mc<=t.bsecond()) mc=1+t.bsecond();
    if(W.size()==n)
      W.push_back(t);
    else W[n++]=t;
  }

  /* Add all tiles from Q */
  void add(const Wang_t &Q) {
    for(int i=0;i<Q.size();i++)
      add(Q[i]);
  }

  /* Construct a Wang set from the graph w1 and w2. (This function is not used.) */
  void t1(const graph_t &w1,const graph_t &w2) {
    assert(N>1);
    n=mc=0;
    for(int i=0;i<N;i++) {
      W[n]=tile_t(w1[i].first(),w1[i].second(),w2[i].first(),w2[i].second());
      if(mc<=w2[i].first()) mc=1+w2[i].first();
      if(mc<=w2[i].second()) mc=1+w2[i].second();
      n++;
    }
  }

  /* remap colors */
  void remap(const map<unsigned int,unsigned int> &m) {
    for(int i=0;i<n;i++)
      W[i]=tile_t(W[i].afirst(),W[i].asecond(),m.at(W[i].bfirst()),m.at(W[i].bsecond()));
  }
  
  /* Sort the tiles */
  void sort() {
    std::sort(W.begin(), W.begin()+n);
    ASSERT(sorted());
  }
  
  /* Returns true if the tiles are sorted */
  bool sorted() const{
    for(int i=1;i<n;i++)
      if(W[i]<W[i-1])
	return false;
    return true;
  }

  /* Removes duplicate tiles. The sorting is a bit slow, so it is not used except for hard tile sets */
  void remove_duplicate() {
    if(!n) return;
    sort();
    int n2=1;
    for(int i=1;i<n;i++) {
      if(!(W[n2-1]==W[i])) {
	if(n2!=i) 
	  W[n2]=W[i];
	n2++;
      }
    }
    n=n2;
  }

  /* return a+b*8, with assertion tests */
  static unsigned int mix(unsigned int a,unsigned int b) {
    ASSERT(a>=0 && b>=0 && a<8);
    unsigned long long r=a+b*8;
    ASSERT(r<(1LL<<32));
    return r;
  }

  /* tn Computes Q*T, where T is the Wang set constructed from the graph w1 and w2 
     On the same time, it removes all sources/sinks of Q*T
     If it returns something > 0, then we aborted since the tile set is not aperiodic.
     Returns OVERFLOW (=-1) if the result is too big (more than MAX tiles).
     Otherwise it returns 0
     
     tn_template<s_t> is a template function, where s_t is a set class (which can be an optimised one, if the number of color is small)
     tn select the good s_t and launch tn_<s_t>
  */
  template<class s_t>
  int ATT_PROF tn_template(const Wang_t &Q,const graph_t &w1,const graph_t &w2) {
    assert(N>1);
    n=0;
    s_t so1,so2;
    for(int i=0;i<Q.n;i++) 
      for(int j=0;j<N;j++)
	if(Q[i].asecond()==w1[j].first()) {
	  int u=mix(w2[j].first(),Q[i].bfirst());
	  so1.insert(u);
	  int v=mix(w2[j].second(),Q[i].bsecond());
	  so2.insert(v);
	  n++;
	}

    /* While there are sources/sinks, we remove them.
       Note that can be very slow for large tilesets (quadratic time), but for small tilesets (huge majority of cases), this is faster than Tarjan's algorithm */
    int old_n=0;
    do {
      old_n=n;
      s_t s1,s2;

      n=0;
      for(int i=0;i<Q.n;i++) 
	for(int j=0;j<N;j++)
	  if(Q[i].asecond()==w1[j].first()) {
	    int u=mix(w2[j].first(),Q[i].bfirst());
	    if(!so2.present(u)) continue;
	    int v=mix(w2[j].second(),Q[i].bsecond());
	    if(!so1.present(v)) continue;
	    s1.insert(u);
	    s2.insert(v);
	    n++;
	  }

      if(n<N) return NA_N_SMALL;
      
      so1.swap(s1);
      so2.swap(s2);
    } while(old_n!=n);

    if(n<=so1.size()) return NA_N_PER_1;
    if(n>W.size()) return OVERFLOW;
    
    n=0;
    mc=so1.size();
    for(int i=0;i<Q.n;i++) 
      for(int j=0;j<N;j++)
	if(Q[i].asecond()==w1[j].first()) {
	  int u=mix(w2[j].first(),Q[i].bfirst());
	  if(!so2.present(u)) continue;
	  int v=mix(w2[j].second(),Q[i].bsecond());
	  if(!so1.present(v)) continue;
	  W[n]=tile_t(Q[i].afirst(),w1[j].second(),so1.position(u),so1.position(v));
	  ASSERT(so1.position(u)<mc && so1.position(v)<mc);
	  n++;
	}
    ASSERT(n==old_n);

    return 0;
  }

  int tn(const Wang_t &wa,const graph_t &w1,const graph_t &w2) {
    for(int i=0;i<wa.n;i++) {
      ASSERT(wa[i].bfirst()>=0 && wa[i].bfirst()<wa.mc);
      ASSERT(wa[i].bsecond()>=0 && wa[i].bsecond()<wa.mc);
    }
#ifndef DO_NOT_USE_BITS
    if(wa.mc<=8)
      return tn_template<set64_t>(wa,w1,w2);
    if(wa.mc<=16)
      return tn_template<set64x_t<2>>(wa,w1,w2);
    if(wa.mc<=32)
      return tn_template<set64x_t<4>>(wa,w1,w2);
    if(wa.mc<=64)
      return tn_template<set64x_t<8>>(wa,w1,w2);
    if(wa.mc<=128)
      return tn_template<set64x_t<16>>(wa,w1,w2);
#endif
    return tn_template<set_t>(wa,w1,w2);
  }

  
  /* Computes Q*T, But we do not remove sources/sink here: 
     The operation of removal of sources/sink is efficient for small Wang sets, but can take too much times for large one (O(n^2) time).
     So we prefer to use tn_nosimpl() on large Wang sets, and run afterward Tarjan's algorithm for strongly connected components
  */
  
  int ATT_PROF tn_nosimpl(const Wang_t &Q,const Wang_t &T) {
    n=0;

    set_t so;
    for(int i=0;i<Q.n;i++) 
      for(int j=0;j<T.n;j++)
	if(Q[i].asecond()==T[j].afirst()) {
	  int u=mix(T[j].bfirst(),Q[i].bfirst());
	  so.insert(u);
	  int v=mix(T[j].bsecond(),Q[i].bsecond());
	  so.insert(v);
	}

    for(int i=0;i<Q.n;i++) 
      for(int j=0;j<T.n;j++)
	if(Q[i].asecond()==T[j].afirst()) {
	  int u=mix(T[j].bfirst(),Q[i].bfirst());
	  int v=mix(T[j].bsecond(),Q[i].bsecond());
	  if(n>=W.size()) return OVERFLOW;
	  W[n++]=tile_t(Q[i].afirst(),T[j].asecond(),so.position(u),so.position(v));
	}
    mc=so.size();;

    return 0;
  }
  
  /* Computes Q*T: we do not remove sources/sink, and we do not relabel colors. We also use base 10 to have a more comprehensible labeling.*/
  
  int ATT_PROF tn_basic(const Wang_t &Q,const Wang_t &T) {
    n=0;
    mc=0;

    for(int i=0;i<Q.n;i++) 
      for(int j=0;j<T.n;j++)
	if(Q[i].asecond()==T[j].afirst()) {
	  int u=T[j].bfirst()+10*Q[i].bfirst();
	  int v=T[j].bsecond()+10*Q[i].bsecond();
	  W[n++]=tile_t(Q[i].afirst(),T[j].asecond(),u,v);
	  if(mc<=u) mc=u+1;
	  if(mc<=v) mc=v+1;
	}
    
    return 0;
  }

  
  /* Computes T^2, where T is the Wang set constructed from the graph w1 and w2 
     Also, remove sources and sinks.
     If it returns something > 0, then we aborted since the tile set is not aperiodic
   */
  int t2(const graph_t &w1,const graph_t &w2) {
    assert(N>1);
    set64_t s1,s2;
    n=0;
    for(int i=0;i<N;i++) 
      for(int j=0;j<N;j++)
	if(w1[i].second()==w1[j].first()) {
	  int u=mix(w2[j].first(),w2[i].first());
	  int v=mix(w2[j].second(),w2[i].second());
	  s1.insert(u);
	  s2.insert(v);
	  n++;
	}

    s1.inter(s2);
    
    while(true) {
      set64_t s1p;
      s2.clear();
      n=0;
      for(int i=0;i<N;i++) 
	for(int j=0;j<N;j++)
	  if(w1[i].second()==w1[j].first()) {
	    int u=mix(w2[j].first(),w2[i].first());
	    if(!s1.present(u)) continue;
	    int v=mix(w2[j].second(),w2[i].second());
	    if(!s1.present(v)) continue;
	    s1p.insert(u);
	    s2.insert(v);
	    n++;
	  }

      if(n<N) return NA_2_SMALL;

      s1p.inter(s2);
      if(s1p.size()<s1.size()) {
	s1=s1p;
	continue;
      }
      break;
    }

    if(n<=s1.size()) return NA_2_PER_1;
    
    n=0;
    for(int i=0;i<N;i++) 
      for(int j=0;j<N;j++)
	if(w1[i].second()==w1[j].first()) {
	  int u=mix(w2[j].first(),w2[i].first());
	  if(!s1.present(u)) continue;
	  int v=mix(w2[j].second(),w2[i].second());
	  if(!s1.present(v)) continue;
	  W[n]=tile_t(w1[i].first(),w1[j].second(),s1.position(u),s1.position(v));
	  n++;
	}
    mc=s1.size();
    
    return 0;
  }

  /* Same as "t2", but we use the index of w1 to speed up */
  int t2_index(const graph_t &w1,const graph_t &w2) {
    assert(N>1);
    set64_t s1,s2;
    n=0;
    for(int i=0;i<N;i++) {
      int c=w1[i].second();
      int en=w1.end(c);
      for(int j=w1.start(c);j<en;j++) {
	ASSERT(w1[i].second()==w1[j].first());
	int u=mix(w2[j].first(),w2[i].first());
	int v=mix(w2[j].second(),w2[i].second());
	s1.insert(u);
	s2.insert(v);
	n++;
      }
    }
    
    s1.inter(s2);
    
    while(true) {
      set64_t s1p;
      s2.clear();
      n=0;
      for(int i=0;i<N;i++) {
	int c=w1[i].second();
	int en=w1.end(c);
	for(int j=w1.start(c);j<en;j++) {
	  ASSERT(w1[i].second()==w1[j].first());
	  int u=mix(w2[j].first(),w2[i].first());
	  if(!s1.present(u)) continue;
	  int v=mix(w2[j].second(),w2[i].second());
	  if(!s1.present(v)) continue;
	  s1p.insert(u);
	  s2.insert(v);
	  n++;
	}
      }
	
      if(n<N) return NA_2_SMALL;

      s1p.inter(s2);
      if(s1p.size()<s1.size()) {
	s1=s1p;
	continue;
      }
      break;
    }
    
    if(n<=s1.size()) return NA_2_PER_1;
    
    n=0;
    for(int i=0;i<N;i++) {
      int c=w1[i].second();
      int en=w1.end(c);
      for(int j=w1.start(c);j<en;j++) {
	ASSERT(w1[i].second()==w1[j].first());
	int u=mix(w2[j].first(),w2[i].first());
	if(!s1.present(u)) continue;
	int v=mix(w2[j].second(),w2[i].second());
	if(!s1.present(v)) continue;
	W[n]=tile_t(w1[i].first(),w1[j].second(),s1.position(u),s1.position(v));
	n++;
      }
    }
    mc=s1.size();

    return 0;
  }

  /* Returns true if the there is a cycle such that the list of colors in the north are the same as the colors in the south (that is, the Wang set is trivilally periodic) */
  bool ATT_PROF is_periodic() const {
    vector<pair<int,int> > Wc;
    Wc.clear();
    for(int i=0;i<n;i++) 
      if(W[i].afirst()==W[i].asecond()) 
	Wc.push_back(make_pair(W[i].bfirst(),W[i].bsecond()));

    return cycle(Wc,mc);
  }

  /* Used by is_periodic(). Returns true iff 'Wc' contains a cycle. Wc must be sorted */ 
  static int ATT_PROF cycle(vector<pair<int,int> > &Wc,int mc) {
    ::sort(Wc.begin(),Wc.end());
    for(int i=0;i<mc;i++) {
      set<int> seen;
      list<int> todo;
      todo.push_back(i);
      while(!todo.empty()) {
	int u=todo.front();
	todo.pop_front();
	if(seen.find(u)!=seen.end()) continue;
	seen.insert(u);
	int st=bsearch(Wc,u);
	ASSERT(st==0 || Wc[st-1].first<u);
	while(st<Wc.size() && Wc[st].first==u) {
	  if(Wc[st].second==i) return true;
	  todo.push_back(Wc[st].second);
	  st++;
	}
      }
    }
    return false;
  }

  /* Used by cycle(). Binary search the first edge in Wc that start with a. Wc must be sorted */
  static int bsearch(const vector<pair<int,int> > &Wc,unsigned int a) {
    int st=0,en=Wc.size();
    while(en>st) {
      int c=(en+st)/2;
      if(Wc[c].first>=a) en=c;
      else st=c+1;
    }
    return st;
  }

  /* Structures for Tarjan's algorithm for strongly connected components */
  struct tarjan_t {
    int index;
    int nsc;
    vector<int> vindex,lowlink,sc,stack,start;
    vector<bool> onstack; 
  } ;
  
  void tarjan(tarjan_t &t,int v) {
    t.vindex[v]=t.lowlink[v]=t.index++;
    t.stack.push_back(v);
    t.onstack[v]=true;

    for(int k=t.start[v];k<n && W[k].bfirst()==v;k++) {
      ASSERT(W[k].bfirst()==v);
      int i=W[k].bsecond();
      if(t.vindex[i]==-1) {
	tarjan(t,i);
	t.lowlink[v]=min(t.lowlink[v],t.lowlink[i]);
      }
      else if(t.onstack[i])
	t.lowlink[v]=min(t.lowlink[v],t.vindex[i]);
    }

    if(t.vindex[v]==t.lowlink[v]) {
      while(true) {
	int w=t.stack.back();
	t.stack.pop_back();
	t.onstack[w]=false;
	t.sc[w]=t.nsc;
	if(v==w) break;
      }
      t.nsc++;
    }
  }

  /* Remove arcs between two different strongly connected components */
  void remove_inter_sc()
  {
    if(!n) return;
    sort();
    
    tarjan_t t;
    t.index=t.nsc=1;
    t.onstack.resize(mc);
    t.vindex.resize(mc,-1);
    t.lowlink.resize(mc);
    t.sc.resize(mc);
    
    t.start.resize(mc);
    long long z=-1;
    for(int i=0;i<n;i++) {
      if(z!=W[i].bfirst()) {
	z=W[i].bfirst();
	t.start[z]=i;
      }
    }
    
    for(int i=0;i<mc;i++)
      if(t.vindex[i]==-1)
	tarjan(t,i);

    int n2=0;
    for(int i=0;i<n;i++) {
      if(t.sc[W[i].bfirst()]==t.sc[W[i].bsecond()]) {
	if(n2!=i) 
	  W[n2]=W[i];
	n2++;
      }
    }
    n=n2;
  }
};

/* Returns a canonical representant of a Wang set */
Wang_t canonical(const Wang_t &g)
{
  int nc=g.num_colors_a();
  int nc2=g.num_colors_b();

  int perm_color[nc];
  for(int i=0;i<nc;i++)
    perm_color[i]=i;
  
  Wang_t m=g;
  Wang_t g2;
  do {
    int perm_color2[nc2];
    for(int i=0;i<nc2;i++)
      perm_color2[i]=i;
    do {
      g2.clear();
      for(int i=0;i<g.size();i++)
	g2.add(tile_t(perm_color[g[i].afirst()],perm_color[g[i].asecond()],perm_color2[g[i].bfirst()],perm_color2[g[i].bsecond()]));

      assert(g2.size()==g.size());
      
      g2.sort();
      if(g2<m)
	m=g2;
    } while(next_permutation(perm_color2,perm_color2+nc2));
  } while(next_permutation(perm_color,perm_color+nc));
  return m;
}

Wang_t canonical_with_mirror(const Wang_t &g) {
  assert(N>1);
  graph_t w1,w2;
  assert(g.size()==N);
  for(int i=0;i<N;i++) {
    w1[i]=edge_t(g[i].afirst(),g[i].asecond());
    w2[i]=edge_t(g[i].bfirst(),g[i].bsecond());
  }
  Wang_t m;
  for(int i=0;i<8;i++) {
    Wang_t t;
    t.t1(w1,w2);
    t=canonical(t);
    if(i==0 || t<m) m=t;
    w1=reverse(w1);
    if((i%2)==1) w2=reverse(w2);
    if((i%4)==3) {auto w3=w1;w1=w2;w2=w3;}
  }
  return m;
}

Wang_t transpose(const Wang_t &g)
{
  Wang_t r;
  for(int i=0;i<g.size();i++)
    r.add(tile_t(g[i].bfirst(),g[i].bsecond(),g[i].afirst(),g[i].asecond()));

  return r;
}

#endif
