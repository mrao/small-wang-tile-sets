#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/resource.h> // for stack 
#include <set>
#include <list>
#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
using namespace std;

long long PROF() {
  struct timespec ts;
  clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts);
  long long en=ts.tv_nsec+1000000000LL*ts.tv_sec;
  return en;
}

#ifndef DEBUG

#define debug 0
#define ASSERT(x) {}

#else

#define debug 1

//additionnal assersions :
#define ASSERT(x) assert(x)
#ifndef PROFILING
#define PROFILING
#endif

#endif

#ifdef PROFILING
//for profiling :
#define ATT_PROF __attribute__ ((noinline))
#else
#define ATT_PROF
#endif

// to avoid stack overflow
void lot_of_stack() {
  struct rlimit rl;
  int res=getrlimit(RLIMIT_STACK,&rl);
  assert(res==0);
  rl.rlim_cur*=100;
  res=setrlimit(RLIMIT_STACK,&rl);
  assert(res==0);
}

#endif
