Here you can find the hardest cases for the test of the non-aperiodicity of Wang sets of size 5..10.

A larger sets of hard cases can be found here : https://www.arthy.org/wang10/

The file 10_band40.txt contains all cases for which we have to compute at least a band of width 40 (i.e. to compute T^40). 10_size10000.txt contains cases for wich we have to compute a transducer of at least 10^5 edges.

There is one result by line: 
N is the number of tiles
g1/g2 are the numbers of the two graphs
#a is the number of candidates found (always 0, except for "the hardest case": N=10 g1=21577 g2=3322)
#t is the number of test for aperiodicity (i.e. call of test_aperiodic() in test.hpp)
date: the unix time when the computation was done 
time: the time for the computation (in ms)
maxband: the largest power we have to compute to get the results
maxsize1/2: the largest transducer we have to compute to get the results (in horizontal, and in vertical)

Note that, for reproductiblity reason, we compute on the same time T^k and (T^tr)^k, and we fail if one of the two is too large (more than MAX edges). This is not optimal, since one of the two can be much smaller than the other.

==

The case "N=10 g1=21577 g2=3322" is special, since it is the only case for which the main program cannot find that there is no aperiodic tileset (discussed in the section "The hardest case" in the paper).

The file "hard10.txt" contains the 4 (isomorphic) tilesets for which the main programm cannot show that there are not aperiodic.




